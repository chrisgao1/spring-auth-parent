package org.schenker.gc.core.spring.authorization;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.mockito.Mockito;
import org.schenker.gc.core.spring.authorization.service.UserService;
import org.schenker.gc.core.spring.authorization.service.impl.UserServiceImpl;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.Resource;

/**
 * Lang: Java
 * ClassName: AuthorizationServerTest
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 12:28 PM
 */
@SpringBootTest
@AutoConfigureMockMvc
public class AuthorizationServerTest {

    @MockBean
    private UserServiceImpl userService;

    @Resource
    private MockMvc mockMvc;

    @Test
    public void testTrial() throws Exception {
        Mockito.doAnswer(o-> o.getArgument(0).hashCode() + o.getArgument(1).hashCode()).when(userService).removeRoles(Mockito.anyString(), Mockito.anyString());
        System.out.println(userService.removeRoles("a", "c"));
        MvcResult mvcResult = this.mockMvc.perform(delete("/user/f/g"))
                .andDo(System.out::println)
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        System.out.println(result);
        System.out.println("OK");
    }
}
