package org.schenker.gc.core.spring.authorization;

import org.junit.jupiter.api.*;
import org.schenker.gc.core.spring.authorization.model.pojo.Role;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.repository.RoleRepository;
import org.schenker.gc.core.spring.authorization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Lang: Java
 * ClassName: H2DatabaseTest
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 9:30 PM
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class H2DatabaseTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Resource
    private MockMvc mockMvc;

    @PostConstruct
    public void init (){
        if (roleRepository.count() == 0) {
            Role role = new Role("admin");
            roleRepository.save(role);

            User user = new User();
            user.setUsername("michael").setPassword("jk").setDeleted(false).setRoles(new ArrayList<>() {{
                add(role);
            }});

            userRepository.save(user);
        }
    }

   /* // 需要加Autowired
    @Autowired
    public H2DatabaseTest(UserRepository userRepository){
        this.userRepository = userRepository;
    }*/

    @Test
    @Order(1)
    public void testQuery() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/user/query")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isOk())
                .andDo(System.out::println)
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @Transactional
    @Order(2)
    public void testRemove(){
        User user = userRepository.findAll().get(0);
        // unit test执行自定义删除有问题(会执行语句，但再次查询数据依然在)，controller执行没有问题
        Integer value = userRepository.removeRoles(user.getId(), user.getRoles().get(0).getId());
        System.out.println(value);
    }

    @Test
    @Transactional
    @Order(3)
    public void testRemoveAfter(){
        User user = userRepository.findAll().get(0);
        System.out.println(user.getRoles());
    }
}
