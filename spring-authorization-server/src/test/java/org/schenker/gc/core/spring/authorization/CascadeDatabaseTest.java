package org.schenker.gc.core.spring.authorization;

import cn.hutool.core.util.ObjectUtil;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.schenker.gc.core.spring.authorization.model.pojo.Department;
import org.schenker.gc.core.spring.authorization.model.pojo.Employee;
import org.schenker.gc.core.spring.authorization.repository.DepartmentRepository;
import org.schenker.gc.core.spring.authorization.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Lang: Java
 * ClassName: H2DatabaseTest
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 9:30 PM
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CascadeDatabaseTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Resource
    private MockMvc mockMvc;

    @PostConstruct
    public void init (){
        if (departmentRepository.count() == 0) {
            Department department = Department.builder().name("sd").level("II").build();
            departmentRepository.save(department); // 如果Employee加了Cascade.Persist,必须不能save

            Employee employee = Employee.builder().name("tmac").department(department)
                    .build();
            employeeRepository.save(employee);

      /*  department.setEmployees(new ArrayList<>(){{add(employee);}});
        departmentRepository.save(department);*/
        }
    }

    @Test
    @Transactional
    @Order(1)
    public void testUpdate() {
        Department d = departmentRepository.findAll().get(0);
        System.out.println(d);
        Employee e = employeeRepository.findAll().get(0);
        System.out.println(e.getDepartment());

        e.getDepartment().setLevel("III");
        employeeRepository.save(e);

        System.out.println(d.getLevel());
        Department d2 = departmentRepository.findAll().get(0);
        System.out.println(d2);
    }

    @Test
    @Transactional
    @Order(2)
    public void testCascadeRemove() throws Exception {
        List<Employee> all = employeeRepository.findAll();
        System.out.println(all.size());

        Department department =  Department.builder().name("sd").build();
        Department one = departmentRepository.findOne(Example.of(department)).orElse(null);
        System.out.println(one);
        // 不能用Objects.nonNull
        if (ObjectUtil.isNotNull(one)) {
            System.out.println(one.getEmployees());
            departmentRepository.delete(one);
        }

        List<Employee> all2 = employeeRepository.findAll();
        System.out.println(all2.size());
    }

    @Test
    @Transactional
    @Order(3)
    public void testRemove(){
        Employee e = employeeRepository.findAll().get(0);
        System.out.println(e.getDepartment());
        Department department =  departmentRepository.findAll().get(0);
        if (department.getEmployees().size() > 0) {
            department.getEmployees().get(0).setDepartment(null);
            department.getEmployees().remove(0); // 同一transaction里需要加这行
            departmentRepository.save(department);

            Employee employee = employeeRepository.findAll().get(0);
            System.out.println(employee.getDepartment());

            Department d2 = departmentRepository.findAll().get(0);
            System.out.println(d2.getEmployees());
        }
    }

    @Test
    @Order(4)
    public void testQuery() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/employee/query"))
                .andDo(System.out::println)
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();
        System.out.println(result);
    }
}
