package org.schenker.gc.core.spring.authorization.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.schenker.gc.core.spring.authorization.model.pojo.Role;
import org.schenker.gc.core.spring.authorization.model.vo.RoleVo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Lang: Java
 * ClassName: RoleMapper
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/2/2022 9:18 PM
 */
@Mapper(componentModel = "spring")
public abstract class RoleMapper {

    abstract RoleVo toRoleVo(Role role);

    abstract Role toRole(RoleVo roleVo);

    abstract List<RoleVo> toRoleVos(List<Role> roles);

    abstract List<Role> toRoles(List<RoleVo> roleVos);

    @Named("getRoles")
    public List<Role> getRoles(List<String> roles){
        // return roles.stream().map(new Role()::setName).collect(Collectors.toList()); // Wrong Way
        return Optional.ofNullable(roles).map(s->s.stream().map(Role::new).collect(Collectors.toList())).orElse(null);
    }
}
