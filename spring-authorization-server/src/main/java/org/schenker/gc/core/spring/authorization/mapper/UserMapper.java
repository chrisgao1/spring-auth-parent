package org.schenker.gc.core.spring.authorization.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.model.to.UserTo;
import org.schenker.gc.core.spring.authorization.model.vo.UserVo;

/**
 * Lang: Java
 * ClassName: RoleMapper
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/2/2022 9:18 PM
 */
@Mapper(componentModel = "spring", uses = { RoleMapper.class })
public interface UserMapper {

    UserVo toUserVo(User user);

    User toUser(UserVo userVo);

    @Mapping(target = "roles", qualifiedByName  = "getRoles")
    User toUser(UserTo userTo);
}
