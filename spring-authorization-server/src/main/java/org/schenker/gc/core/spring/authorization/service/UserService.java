package org.schenker.gc.core.spring.authorization.service;

import org.schenker.gc.core.spring.authorization.model.to.UserTo;
import org.schenker.gc.core.spring.authorization.model.vo.UserVo;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;

/**
 * Lang: Java
 * ClassName: UserService
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/2/2022 5:45 PM
 */
public interface UserService {
    PagedModel<UserVo> query(UserTo userTo, Pageable pageable);
    Integer removeRoles(String userId, String roleId);
}
