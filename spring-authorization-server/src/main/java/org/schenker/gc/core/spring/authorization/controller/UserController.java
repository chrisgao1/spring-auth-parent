package org.schenker.gc.core.spring.authorization.controller;

import org.schenker.gc.core.spring.authorization.assembler.UserAssembler;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.model.to.UserTo;
import org.schenker.gc.core.spring.authorization.model.vo.UserVo;
import org.schenker.gc.core.spring.authorization.repository.UserRepository;
import org.schenker.gc.core.spring.authorization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Lang: Java
 * ClassName: UserController
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/26/2022 1:47 PM
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("query")
    public ResponseEntity query(@RequestBody UserTo userTo, Pageable pageable){
        PagedModel<UserVo> page = userService.query(userTo, pageable);
        return ResponseEntity.ok(page);
    }

    @RequestMapping("{userId}/{roleId}")
    @Transactional
    public ResponseEntity removeRole(@PathVariable("userId") String userId, @PathVariable("roleId") String roleId){
        Integer rowcount = userService.removeRoles(userId, roleId);
        if (rowcount > 0){
            return ResponseEntity.ok("ok");
        }
        return ResponseEntity.notFound().build();
    }
}
