package org.schenker.gc.core.spring.authorization.controller;

import org.schenker.gc.core.spring.authorization.model.pojo.Department;
import org.schenker.gc.core.spring.authorization.model.pojo.Employee;
import org.schenker.gc.core.spring.authorization.repository.DepartmentRepository;
import org.schenker.gc.core.spring.authorization.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Lang: Java
 * ClassName: UserController
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/26/2022 1:47 PM
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @RequestMapping("query")
    public ResponseEntity query(Pageable pageable){
        List<Employee> all = employeeRepository.findAll();
        System.out.println(all.size());

        Department department =  Department.builder().name("sd").build();
        Department one = departmentRepository.findOne(Example.of(department)).orElse(null);
        Assert.notNull(one, "department is null");
        System.out.println(one);
        System.out.println(one.getEmployees());

        return ResponseEntity.ok(all);
    }
}
