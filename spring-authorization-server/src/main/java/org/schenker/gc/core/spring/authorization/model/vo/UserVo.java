package org.schenker.gc.core.spring.authorization.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.schenker.gc.core.spring.infrastructure.model.dto.BaseTo;
import org.springframework.hateoas.server.core.Relation;

import java.util.ArrayList;
import java.util.List;

/**
 * Lang: Java
 * ClassName: UserVo
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/2/2022 5:46 PM
 */
@Data
@ToString
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(collectionRelation = "users", itemRelation = "user")
@NoArgsConstructor
@AllArgsConstructor
public class UserVo extends BaseTo<UserVo> {

    @JsonProperty("id")
    @ApiModelProperty(value = "id")
    private String id;

    @JsonProperty("username")
    @ApiModelProperty(value = "username")
    private String username;

    @JsonProperty("password")
    @ApiModelProperty(value = "password")
    private String password;

    @JsonProperty("roles")
    @ApiModelProperty(value = "roles")
    private List<RoleVo> roles = new ArrayList<>();

    @JsonIgnore
    private boolean deleted;
}
