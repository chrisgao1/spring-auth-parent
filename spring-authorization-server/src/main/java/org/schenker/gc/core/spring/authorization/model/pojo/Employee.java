package org.schenker.gc.core.spring.authorization.model.pojo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.schenker.gc.core.spring.infrastructure.model.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Lang: Java
 * ClassName: Department
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 2/24/2023 11:33 AM
 */
@Data
@Accessors(chain = true)
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_employee")
public class Employee extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String name;

    @JsonBackReference
    // @ManyToOne(cascade = CascadeType.PERSIST)
    @ManyToOne
    @JoinColumn(name = "department", referencedColumnName = "name")
    private Department department;

    @Override
    public String toString() {
        return "Employee{id=" + id + ", name=" + name + "}";
    }
}
