package org.schenker.gc.core.spring.authorization.repository;

import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.infrastructure.repository.BaseJpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Lang: Java
 * ClassName: UserRepository
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 10:07 AM
 */
public interface UserRepository extends BaseJpaRepository<User, String> {
    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from tbl_user_role where user_id = ?1 and role_id = ?2")
    Integer removeRoles(String userId, String roleId);
}
