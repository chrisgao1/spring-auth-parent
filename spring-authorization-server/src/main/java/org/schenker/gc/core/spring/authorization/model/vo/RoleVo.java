package org.schenker.gc.core.spring.authorization.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.schenker.gc.core.spring.infrastructure.model.dto.BaseTo;
import org.springframework.hateoas.server.core.Relation;

/**
 * Lang: Java
 * ClassName: UserVo
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/2/2022 5:46 PM
 */
@Data
@ToString
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(collectionRelation = "roles", itemRelation = "role")
@NoArgsConstructor
@AllArgsConstructor
public class RoleVo extends BaseTo<RoleVo> {

    @JsonProperty("id")
    @ApiModelProperty(value = "id")
    private String id;

    @JsonProperty("name")
    @ApiModelProperty(value = "name")
    private String name;

    @JsonIgnore
    private boolean deleted;
}
