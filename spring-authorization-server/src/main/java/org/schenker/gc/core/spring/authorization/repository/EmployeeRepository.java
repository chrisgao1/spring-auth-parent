package org.schenker.gc.core.spring.authorization.repository;

import org.schenker.gc.core.spring.authorization.model.pojo.Department;
import org.schenker.gc.core.spring.authorization.model.pojo.Employee;
import org.schenker.gc.core.spring.infrastructure.repository.BaseJpaRepository;

/**
 * Lang: Java
 * ClassName: UserRepository
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 10:07 AM
 */
public interface EmployeeRepository extends BaseJpaRepository<Employee, String> {
}
