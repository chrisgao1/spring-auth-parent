package org.schenker.gc.core.spring.authorization.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.schenker.gc.core.spring.infrastructure.model.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Lang: Java
 * ClassName: User
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/23/2022 11:04 PM
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "tbl_role", uniqueConstraints = @UniqueConstraint(name = "tbl_role_name_key", columnNames = {"name"}))
@SQLDelete(sql = "UPDATE tbl_role SET deleted=true WHERE id=?")
@Where(clause = "deleted = false")
@NoArgsConstructor
public class Role extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private String name;
    private boolean deleted;

    public Role(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{id=" + id + ", name=" + name + "}";
    }
}
