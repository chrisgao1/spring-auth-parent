package org.schenker.gc.core.spring.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Lang: Java
 * ClassName: AuthorizationServer
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/18/2022 1:26 PM
 */
@SpringBootApplication
public class AuthorizationServer {
    public static void main(String[] args) {
        SpringApplication.run(AuthorizationServer.class);
    }
}
