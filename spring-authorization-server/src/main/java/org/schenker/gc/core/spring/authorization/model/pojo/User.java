package org.schenker.gc.core.spring.authorization.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.schenker.gc.core.spring.infrastructure.model.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Lang: Java
 * ClassName: User
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/23/2022 11:04 PM
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "tbl_user", uniqueConstraints = @UniqueConstraint(name = "tbl_user_username_key", columnNames = {"username"}))
@SQLDelete(sql = "UPDATE tbl_user SET deleted=true WHERE id=?")
@Where(clause = "deleted = false")
public class User extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private String username;
    private String password;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "tbl_user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles = new ArrayList<>();

    private boolean deleted;

    @Override
    public String toString() {
        return "User{id=" + id + ", name=" + username + "}";
    }
}
