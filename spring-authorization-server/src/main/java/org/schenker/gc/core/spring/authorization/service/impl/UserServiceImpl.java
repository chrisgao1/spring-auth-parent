package org.schenker.gc.core.spring.authorization.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.schenker.gc.core.spring.authorization.assembler.UserAssembler;
import org.schenker.gc.core.spring.authorization.mapper.UserMapper;
import org.schenker.gc.core.spring.authorization.model.pojo.Role;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.model.to.UserTo;
import org.schenker.gc.core.spring.authorization.model.vo.UserVo;
import org.schenker.gc.core.spring.authorization.repository.UserRepository;
import org.schenker.gc.core.spring.authorization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 * Lang: Java
 * ClassName: UserServiceImpl
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/3/2022 12:59 AM
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserAssembler userAssembler;

    @Resource
    private PagedResourcesAssembler<User> pagedResourcesAssembler;

    @Resource
    private UserRepository userRepository;

    @Override
    public PagedModel<UserVo> query(UserTo userto, Pageable pageable) {
        User user = userMapper.toUser(userto);
        log.info(user.toString());

        Specification<User> spec = (root, criteriaQuery, cb) -> {
            Predicate pre = cb.conjunction();
            pre.getExpressions().add(cb.isFalse(root.get("deleted")));
            Join<User, Role> join = root.join("roles", JoinType.INNER);
            pre.getExpressions().add(cb.isFalse(join.get("deleted")));

            if (!ObjectUtils.isEmpty(user.getUsername())){
                pre.getExpressions().add(cb.like(root.get("username"), "%" + user.getUsername() + "%"));
            }

            if (!CollectionUtils.isEmpty(userto.getRoles())) {
                CriteriaBuilder.In<String> in = cb.in(join.get("name"));
                userto.getRoles().forEach(s->{
                    in.value(s);
                });
                pre.getExpressions().add(in);
            }

            return pre;
        };
        Page<User> page = userRepository.findAll(spec, pageable);

        return pagedResourcesAssembler.toModel(page, userAssembler);
    }

    @Override
    public Integer removeRoles(String userId, String roleId) {
        return userRepository.removeRoles(userId, roleId);
    }
}
