/*
 * @COPYRIGHT (C) 2022 Schenker AG
 *
 * All rights reserved
 */


package org.schenker.gc.core.spring.authorization.assembler;

import org.schenker.gc.core.spring.authorization.mapper.UserMapper;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.model.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * (Cost)
 * <p>
 * Created by author@dbschenker.com on 2022-03-29 11:20:16.
 */
@Component
public class UserAssembler extends RepresentationModelAssemblerSupport<User, UserVo> {

    @Resource
    private UserMapper userMapper;

    public UserAssembler(Class<?> controllerClass,
                         Class<UserVo> resourceType) {
        super(controllerClass, resourceType);
    }

    @Autowired
    public UserAssembler() {
        super(User.class, UserVo.class);
    }

    @Override
    public UserVo toModel(User user) {
        UserVo userVo = userMapper.toUserVo(user);
        userVo.add(Link.of("/user").withSelfRel().expand(userVo.getId()));
        return userVo;
    }

    @Override
    public CollectionModel<UserVo> toCollectionModel(Iterable<? extends User> entities) {
        return super.toCollectionModel(entities);
    }
}
