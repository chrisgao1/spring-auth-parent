package org.schenker.gc.core.spring.authorization.config;

import lombok.extern.slf4j.Slf4j;
import org.schenker.gc.core.spring.authorization.model.pojo.User;
import org.schenker.gc.core.spring.authorization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Lang: Java
 * ClassName: SpringApplicationRunner
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 11:59 AM
 */
@Slf4j
@Configuration
public class SpringApplicationRunner implements ApplicationRunner {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void run(ApplicationArguments args) throws Exception {
        log.info("Application start {}", new Date());
    }
}
