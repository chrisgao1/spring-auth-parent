package org.schenker.gc.core.spring.authorization.model.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Access;
import java.util.List;

/**
 * Lang: Java
 * ClassName: UserTo
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/3/2022 10:30 AM
 */
@Data
@ToString
@Accessors(chain = true)
public class UserTo {

    @JsonProperty("id")
    @ApiModelProperty(value = "id")
    private String id;

    @JsonProperty("username")
    @ApiModelProperty(value = "username")
    private String username;

    @JsonProperty("password")
    @ApiModelProperty(value = "password")
    private String password;

    @JsonProperty("roles")
    @ApiModelProperty(value = "roles")
    private List<String> roles;
}
