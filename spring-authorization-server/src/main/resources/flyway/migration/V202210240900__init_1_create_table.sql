CREATE TABLE IF NOT EXISTS auth_server.tbl_role (
                                  id varchar(36) PRIMARY KEY NOT NULL,
                                  name varchar(100) NOT NULL UNIQUE,
                                  deleted bool DEFAULT false
);