DROP TABLE IF EXISTS auth_server.tbl_user;

CREATE TABLE auth_server.tbl_user (
                                  id varchar(36) PRIMARY KEY NOT NULL,
                                  username varchar(100) NOT NULL UNIQUE,
                                  password varchar(100) NOT NULL,
                                  deleted bool DEFAULT false,
                                  create_time timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                  update_time timestamp NULL DEFAULT CURRENT_TIMESTAMP
);