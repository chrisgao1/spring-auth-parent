package org.schenker.gc.core.spring.infrastructure.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Date;

/**
 * Lang: Java
 * ClassName: BaseTo
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 11/3/2022 12:42 AM
 */
@Data
public class BaseTo<T extends BaseTo<? extends T>> extends RepresentationModel<T> implements
        Serializable {

    @JsonProperty("createTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "createTime")
    private Date createTime;

    @JsonProperty("createUserId")
    @ApiModelProperty(value = "createUserId")
    private String createUserId;

    @JsonProperty("updateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "updateTime")
    private Date updateTime;

    @JsonProperty("updateUserId")
    @ApiModelProperty(value = "updateUserId")
    private String updateUserId;
}
