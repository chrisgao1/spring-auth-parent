package org.schenker.gc.core.spring.infrastructure.model.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * Lang: Java
 * ClassName: BaseEntity
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/25/2022 9:30 AM
 */
@Data
@MappedSuperclass
@Accessors(chain = true)
@SuppressWarnings("serial")
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity {
    // create_time
    @CreatedDate
    private Date createTime;

    // update_time
    @LastModifiedDate
    private Date updateTime;

    // create_user_id
    @CreatedBy
    private String createUserId;

    // update_user_id
    @LastModifiedBy
    private String updateUserId;
}
