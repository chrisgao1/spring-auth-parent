package org.schenker.gc.core.spring.infrastructure.repository;

import org.schenker.gc.core.spring.infrastructure.model.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Lang: Java
 * ClassName: BaseJpaRepository
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 10/24/2022 11:43 AM
 */
public interface BaseJpaRepository<T extends BaseEntity, String> extends JpaRepository<T, String>, JpaSpecificationExecutor<T> {
}
